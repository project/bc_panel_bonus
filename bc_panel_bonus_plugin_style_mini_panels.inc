<?php

class bc_panel_bonus_plugin_style_mini_panels extends views_plugin_style {  
  function render() {
  	$i = 0 ;
  	
  	$display = $this->view->display[$this->view->current_display] ;
  	$options = $display->handler->options ;
  	$perPage = $this->options['settings_per_page'] ;

  	foreach ($this->view->result as $row) {
  		$i++ ;
  		$node = node_load($row->nid);
  		$display = $this->options['row_' . $i];
  			if ( $display != "0" && ($perPage == "0" && $this->view->pager['current_page']  == "0" ) ) {
  				$node->build_mode = BC_PANELS_BUILD_MODE_PREFIX . $display ;
  			} elseif ( $display != "0" ) {
  				$node->build_mode = BC_PANELS_BUILD_MODE_PREFIX . $display ;
  			}
  			if ( $node->sticky && $this->options['sticky'] != "0" ) {
  				$node->build_mode = BC_PANELS_BUILD_MODE_PREFIX . $this->options['sticky'] ;
  			}
      	$rows .= node_view($node,true,false);
    }
    return $rows ;
  }

  function options_form(&$form, &$form_state) {
  	parent::options_form($form, $form_state);
  	$view = $this->view;
  	$currentDisplay = $view->current_display;
  	$display = $view->display[$currentDisplay] ;
  	$options = $display->handler->options ;
  	
  	$minis = bc_panel_bonus_get_mini_panels();
  	array_unshift($minis ,  t('Default') );
  	
  	$savedOptions = $this->options ; 
  	
  	$form['settings_per_page'] = array(
  		'#type' => 'checkbox',
  		'#title' => t('Per page settings'),
  		'#default_value' => $savedOptions['settings_per_page']
  	);
	  	
  	$form['row'] = array(
  		'#tree' => true,
  		'#type' => 'fieldset'
  	);

  	for ( $i = 1 ; $i <= $options['items_per_page'] ; $i++ ) {
  		$form['row_'.$i] = array(
  			'#type' => 'select' ,
  			'#title' => t('Row !i', array('!i' => $i )),
  			'#options' => $minis ,
  			'#default_value' => $savedOptions['row_' . $i] ,
  			'#tree' => true
  		);
  	}
  	
  	$form['sticky'] = array(
  		'#type' => 'select',
  		'#options' => $minis ,
  		'#title' => t('Override sticky nodes'),
  		'#default_value' => $savedOptions['sticky']
  	);
  }
  
}